# UA_genedoe

Here you can find code for the analysis of data in the GeneDoe project.
Source code can be found in the jupyter notebook files in `/code`

1. `analysis_plan_cleaned.ipynb`: The main statistical analyses of the GeneDoe project
2. `differential_abundance.ipynb`: The differential abundance tests performed in the GeneDoe project
3. `predict_intercourse_genedoe_isala_rapekit.ipynb`: Intercourse predictions performed within the Isala, GeneDoe and Rapekit datasets

Preprint available at: [https://dx.doi.org/10.21203/rs.3.rs-4302243/v1](https://dx.doi.org/10.21203/rs.3.rs-4302243/v1)
